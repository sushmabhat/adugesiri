<?php
/* Title : Ajax paginatn with jQuery & PHP
Example URL : http://www.sanwebe.com/2013/03/ajax-paginatn-with-jquery-php */

//continue only if $_POST is set and it is a Ajax request
if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
	
	include("config.inc.php");  //include config file
	//Get page number from Ajax POST
	if(isset($_POST["page"])){
		$page_number = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
		if(!is_numeric($page_number)){die('Invalid page number!');} //incase of invalid page number
	}else{
		$page_number = 1; //if there's no page number, set it to 1
	}
	
	//get total number of records from database for paginatn
	$results = $mysqli_conn->query("SELECT COUNT(*) FROM entries");
	
	$get_total_rows = $results->fetch_row(); //hold total records in variable
	
	echo 'count-', $get_total_rows[0];
	
	//break records into pages
	$total_pages = ceil($get_total_rows[0]/$item_per_page);
	
	//get starting position to fetch the records
	$page_position = (($page_number-1) * $item_per_page);
	
	//SQL query that will fetch group of records depending on starting position and item per page. See SQL LIMIT clause
	$results = $mysqli_conn->query("SELECT id, title, contents, mainfile FROM entries ORDER BY id ASC LIMIT $page_position, $item_per_page");
	
	while($row = $results->fetch_assoc()) {
	
	?>
	<div class="blog-entry">
                        <div class="entry-cover">
                            <a href="post.php?id=<?php $id = $row["id"]; print "$id";?>"><img src="images/cook/<?php $mainfile = $row["mainfile"]; print "$mainfile";?>" alt="blog image" /></a>
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <div class="entry-header">
                                <h1><a href="post.php"><?php $title = $row1["title"]; print "$title";?></a></h1>
                                <p><span><?php $date1 = $row["currdate"]; print $date1; ?></span> by <a href="#">Reshma</a> </p>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="entry-content">
                                    <p><?php $content = $row["content"]; print "$content";?></p>
                                </div>
                            </div>
                        </div>
    </div>
	
	<?php 
	}
	/* We call the paginatn function here to generate paginatn link for us. 
	As you can see I have passed several parameters to the function. */
	//echo 'item per page-', $item_per_page, 'page no-', $page_number, 'total rows -', $get_total_rows[0], 'total pages - ', $total_pages;
	echo paginate_function($item_per_page, $page_number, $get_total_rows[0], $total_pages);
	echo '</div>';
}
################ paginatn function #########################################
function paginate_function($item_per_page, $current_page, $total_records, $total_pages)
{
    $paginatn = '';
	
	if($total_pages > 0 && $total_pages != 1 && $current_page <= $total_pages){ //verify total pages and current page number
        $paginatn .= '<ul class="paginatn">';
        
        $right_links    = $current_page + 3; 
        $previous       = $current_page - 3; //previous link 
        $next           = $current_page + 1; //next link
        $first_link     = true; //boolean var to decide our first link
        
        if($current_page > 1){
			$previous_link = ($previous==0)?1:$previous;
            $paginatn .= '<li class="first"><a href="#" data-page="1" title="First"></a></li>'; //first link
            $paginatn .= '<li><a href="#" data-page="'.$previous_link.'" title="Previous">&lt;</a></li>'; //previous link
                for($i = ($current_page-2); $i < $current_page; $i++){ //Create left-hand side links
                    if($i > 0){
                        $paginatn .= '<li><a href="#" data-page="'.$i.'" title="Page'.$i.'">'.$i.'</a></li>';
                    }
                }   
            $first_link = false; //set first link to false
        }
        
        if($first_link){ //if current active page is first link
            $paginatn .= '<li class="first active">'.$current_page.'</li>';
        }elseif($current_page == $total_pages){ //if it's the last active link
            $paginatn .= '<li class="last active">'.$current_page.'</li>';
        }else{ //regular current link
            $paginatn .= '<li class="active">'.$current_page.'</li>';
        }
                
        for($i = $current_page+1; $i < $right_links ; $i++){ //create right-hand side links
            if($i<=$total_pages){
                $paginatn .= '<li><a href="#" data-page="'.$i.'" title="Page '.$i.'">'.$i.'</a></li>';
            }
        }
        if($current_page < $total_pages){ 
				$next_link = ($i > $total_pages)? $total_pages : $i;
                $paginatn .= '<li><a href="#" data-page="'.$next_link.'" title="Next">&gt;</a></li>'; //next link
                $paginatn .= '<li class="last"><a href="#" data-page="'.$total_pages.'" title="Last">&raquo;</a></li>'; //last link
        }
        
        $paginatn .= '</ul>'; 
    }
	
    return $paginatn; //return paginatn links
}
?>

