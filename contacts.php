<?php

/*
  |--------------------------------------------------------------------------
  | Mailer module
  |--------------------------------------------------------------------------
  |
  | These module ares used when sending email from contact form
  |
 */

// Swift Mailer Library
require_once 'swiftmailer/lib/swift_required.php'; 

/* SECTION I - CONFIGURATION */

// Mail Transport
$transport = Swift_SmtpTransport::newInstance('ssl://smtp.gmail.com', 465)
    ->setUsername('sushmabhat.shimoga@gmail.com') // Your Gmail Username
    ->setPassword('M19ad3zf2_TCS'); // Your Gmail Password
	
// Mailer
$mailer = Swift_Mailer::newInstance($transport);

$receiver_mail = 'sushmabhat.shimoga@gmail.com';

$mail_title    = ( ! empty( $_POST[ 'website' ] )) ? $_POST[ 'contact-name' ] . ' from ' . $_POST[ 'website' ] : ' from [WebSite]';

echo "mail ", $mail_title ;

// Create a message
$message = Swift_Message::newInstance($mail_title)
    ->setFrom(array($_POST [ 'contact-email' ])) // can be $_POST['email'] etc...
    ->setTo(array($receiver_mail)) // your email / multiple supported.
    ->setBody($_POST [ 'contact-message' ]);

// Send the message
if ($mailer->send($message)) {
    $result = 'Message successfully sent.';
} else {
    $result  = 'Please fill all the fields in the form.';
}	
echo $result;