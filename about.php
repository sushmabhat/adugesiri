<?php
//This code was written by Freddie Sherratt
//and has been lisenced under the creative commons act.
//See http://creativecommons.org/licenses/by/2.0/uk/ for licsencing agreement.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

//The line of code below runs the connect script, this allows you to connect to your sql datebase throughout the rest of the code.
include_once"scripts/connect.php";
//Tells the computer only to run this code if the form has been submited

$parse_var = '';
$msg = '';


//The next 3 lines gather the information entered into form and turns them into php variables so they can be entered into the database
if (isset($_POST['parse_var'])){
  if ($_POST['parse_var'] == "new"){
	 	
	$name    = isset($_POST['name']) ? $_POST['name'] : '';
	$email   = isset($_POST['email']) ? $_POST['email'] : '';
	$website = isset($_POST['website']) ? $_POST['website'] : '';
	$comment = isset($_POST['comment']) ? $_POST['comment'] : '';	
	
	//$sqlcreate = mysql_query("INSERT INTO comments (name, email, website, comments)
	//VALUES('$name','$email','$website', '$comment')");
	
	$conn = new mysqli($db_host, $db_username, $db_pass, $db_name);
	$stmt = $conn->prepare("INSERT INTO comments (name, email, website, comments) VALUES(?,?,?,?)");
	$stmt->bind_param("ssss", $name, $email, $website, $comment);
	$stmt->execute();
	
	/*if ($sqlcreate){
		$msg = '<font color="#009900">A new comment has been posted.</font>';
	} else {
		$msg = '<font color="#FF0000">Problems connecting to server, please try again later.</font>';
	}*/
	
	echo "New records created successfully";

	$stmt->close();
	$conn->close();
  }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>AdugeSiri - Authentic & Modern Indian Cooking</title>
        <meta name="description" content="Here goes description" />
        <meta name="author" content="Reshma" />
        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
        <!-- ======================================================================
                                    Mobile Specific Meta
        ======================================================================= -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <!-- ======================================================================
                                    Style CSS + Google Fonts
        ======================================================================= -->
        <link href='http://fonts.googleapis.com/css?family=Oswald:400,700,300|Bitter:400,700,400italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/bootstrap.css" />    
        <link rel="stylesheet" href="css/style.css" />
    </head>
<body>
    <!-- ===============================================
    START HEADER =================================== -->
    <div class="header">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-xs-6">
                    <div class="logo">
						<?php
						//this is where the response message will be printed
						 print"$msg";
						 ?>
                        <a href="index.html">AdugeSiri</a>
                    </div>
                </div>
                <div class="col-md-8 col-xs-6">
                    <div class="menu">
                        <div class="responsive-menu">Menu</div>
                        <ul>
                            <li><a href="index.html">Home</a></li>
                            <li><a href="gallery.html">Recipe gallery</a></li>
                            <li class="active"><a href="about.html">About</a></li>
                            <li><a href="contact.html">Contact</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="header-heading">
            <h2>Your recipe is at a click away. Choose your favorite Karnataka/Indian recipe and cooking with AdugeSiri. Enjoy all the flavors with AdugeSiri.</h2>
            </div>
        </div>
    </div>
    <!-- ===============================================
    END HEADER =================================== -->



    <!-- ===============================================
    START CONTENT =================================== -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="page">
                        <h3 class="site-title">A little about me</h3>
                        <img src="images/author.jpg" alt="about" />
                        <p>My name is Reshma, and i like to cook, and i like... Vivamus gravida justo sed nisl viverra in venenatis lacus posuere. Suspendisse iaculis turpis gravida neque ullamcorper malesuada.</p>
                        
                        <hr/>
                            <div class="comments-area">
								<?php
									@mysql_connect("$db_host","$db_username","$db_pass") or die ("could not connect to mysql");
									@mysql_select_db("$db_name") or die ("no database");
									$sqlcounter = mysql_query("SELECT count(*) as counter from comments");
									while($row1 = mysql_fetch_array($sqlcounter)){
										$counter = $row1["counter"];
									?>
									<h3><?php print"$counter"; ?> people commented</h3>
									<?php } ?>
									<ul class="commentlist">                                     
											<?php  
											$sqlfetch = mysql_query("select name, comments, website, website, date_format(updateddate, '%b %d %Y') date, date_format(updateddate, '%h:%i %p') time from comments");
											while($row1 = mysql_fetch_array($sqlfetch)){
												?>
												<li>
													<div class="comment">
														<p><?php $comment = $row1["comments"]; print "$comment"; ?></p>
														<span class="comment-info">
														<i>by</i> <?php $name = $row1["name"]; print "$name"; ?> <i>on</i> 
														<span><?php $date1 = $row1["date"]; print $date1; ?></span> 
														<i>at</i> <?php print $time1 = $row1["time"]; print $time1; ?>- <a href="#">Reply</a>
														</span>	
													</div> 
												 </li>	
											<?php } ?>
									</ul>

                                <div id="respond" class="comment-respond">
                                <h3>Tell me something</h3>
                                    <form action="#" method="post" id="commentform" class="comment-form" action="about.php">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <p>Name</p><input class="comments-line" name="name" type="text">
                                            </div>
                                            <div class="col-md-4">
                                                <p>E-mail</p><input class="comments-line" name="email" type="text">
                                            </div>
                                            <div class="col-md-4">
                                                <p>Website</p><input class="comments-line" name="website" type="text">
                                            </div>
                                        </div>
                                        <p>Comment</p>
                                        <textarea class="comments-area" name="comment"></textarea>
                                        <p class="form-submit">
                                            <input name="submit" type="submit" id="submit" value="post comment" class="comments-button">
                                            <input type="hidden" name="comment_post_ID" value="" id="comment_post_ID">
                                            <input type="hidden" name="comment_parent" id="comment_parent" value="0">
											<input name="parse_var" type="hidden" value="new" />
                                        </p>
                                    </form>
                                </div>
                            </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="sidebar">
                        <div class="widget">
                            <form class="search">
                                <input type="text" class="search-line" placeholder="What are you searching for?" name="serch">
                                <input type="submit" value="" class="search-button">
                            </form>
                        </div>

                        <div class="widget">
                            <h2 class="widget-title">Recent Recipies</h2>
                            <div class="popular-entrys">
								<?php
								$populars   = mysql_query("select id, title, mainfile from entries");
								while($row1 = mysql_fetch_array($populars)){
								?>
                                <div class="popular-entry">
                                    <a href="post.php?id=<?php $id = $row1["id"]; print "$id";?>"><img src="images/cook/<?php $mainfile = $row1["mainfile"]; print "$mainfile";?>" alt="blog image" /></a>
                                    <h3><a href="post.php?id=<?php $id = $row1["id"]; print "$id";?>"><?php $title = $row1["title"]; print "$title";?></a></h3>
                                </div>
								<?php
								}
								?>                                
                            </div>
                        </div>

                        <div class="widget">
                            <h2 class="widget-title">Social</h2>
                            <ul class="socials">
                                <li><a href="#"><img src="images/mini_facebook.png" alt="facebook" /></a></li>
                                <li><a href="#"><img src="images/mini_twitter.png" alt="twitter" /></a></li>
                                <li><a href="#"><img src="images/mini_rss.png" alt="rss" /></a></li>
                                <li><a href="#"><img src="images/mini_google+.png" alt="google+" /></a></li>
                            </ul>
                        </div>

                        <div class="widget">
                            <h2 class="widget-title">Meta</h2>
                            <ul>
                                <li><a href="#">Log in</a></li>
                                <li><a href="#">Archive</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ===============================================
    END CONTENT =================================== -->



    <!-- ===============================================
    START FOOTER =================================== -->
    <div class="container">
        <div class="footer">
            <p>Copyright © 2014 iCOOK Blog. All rights reserved. Vivamus gravida justo sed nisl viverra in venenatis lacus posuere.</p>
            <ul>
                <li><a href="about.html">About</a></li>
                <li><a href="recipe.html">Recipe Gallery</a></li>
                <li><a href="contact.html">Contact</a></li>
            </ul>
        </div>
    </div>
    <!-- ===============================================
    END FOOTER =================================== -->

        <!-- ======================================================================
                                        START SCRIPTS
        ======================================================================= -->
        <script src="js/modernizr.custom.63321.js" type="text/javascript"></script>
        <script src="js/jquery-1.10.0.min.js" type="text/javascript"></script>
        <script src="js/jquery-ui.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.js" type="text/javascript"></script>
        <script src="js/placeholder.js" type="text/javascript"></script>
        <script src="js/imagesloaded.pkgd.min.js" type="text/javascript"></script>
        <script src="js/masonry.pkgd.min.js" type="text/javascript"></script>
        <script src="js/jquery.swipebox.min.js" type="text/javascript"></script>
        <script src="js/options.js" type="text/javascript"></script>
        <script src="js/plugins.js" type="text/javascript"></script>
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <!-- ======================================================================
                                        END SCRIPTS
        ======================================================================= -->
    </body>
</html>