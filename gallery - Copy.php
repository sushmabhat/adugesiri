<!DOCTYPE HTML>
<html lang="en">
	<head>
	<meta charset="utf-8" />
	<title>Adugesiri</title>
	<meta name="description" content="Adugesiri contains loads of recipies across Karnataka and India" />
	<meta name="author" content="Reshma Bhat" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
	<!-- ======================================================================
								Mobile Specific Meta
	======================================================================= -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<!-- ======================================================================
								Style CSS + Google Fonts
	======================================================================= -->
	<link href='http://fonts.googleapis.com/css?family=Oswald:400,700,300|Bitter:400,700,400italic' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/bootstrap.css" />    
	<link rel="stylesheet" href="css/style.css" />
	<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {
		$("#results" ).load( "fetch_pages.php"); //load initial records
		
		//executes code below when user click on paginatn links
		$("#results").on( "click", ".paginatn a", function (e){
			e.preventDefault();
			$(".loading-div").show(); //show loading element
			var page = $(this).attr("data-page"); //get page number from link
			$("#results").load("fetch_pages.php",{"page":page}, function(){ //get content from PHP page
				$(".loading-div").hide(); //once done, hide loading element
			});
			
		});
	});
	</script>
	<style>

	.contents{
		margin: 20px;
		padding: 20px;
		list-style: none;
		background: #F9F9F9;
		border: 1px solid #ddd;
		border-radius: 5px;
	}
	.contents li{
		margin-bottom: 10px;
	}
	.loading-div{
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		background: rgba(0, 0, 0, 0.56);
		z-index: 999;
		display:none;
	}
	.loading-div img {
		margin-top: 20%;
		margin-left: 50%;
	}

	/* paginatn style */
	.paginatn{margin:0;padding:0;}
	.paginatn li{
		display: inline;
		padding: 6px 10px 6px 10px;
		border: 1px solid #ddd;
		margin-right: -1px;
		font: 15px/20px Arial, Helvetica, sans-serif;
		background: #FFFFFF;
		box-shadow: inset 1px 1px 5px #F4F4F4;
	}
	.paginatn li a{
		text-decoration:none;
		color: rgb(89, 141, 235);
	}
	.paginatn li.first {
		border-radius: 5px 0px 0px 5px;
	}
	.paginatn li.last {
		border-radius: 0px 5px 5px 0px;
	}
	.paginatn li:hover{
		background: #CFF;
	}
	.paginatn li.active{
		background: #F0F0F0;
		color: #333;
	}
	</style>
</head>
<body>
<!-- ===============================================
    START HEADER =================================== -->
    <div class="header">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-xs-6">
                    <div class="logo">
                        <a href="index.html">Adugesiri</a>
                    </div>
                </div>
                <div class="col-md-7 col-xs-6">
                    <div class="menu">
                        <div class="responsive-menu">Menu</div>
                        <ul>
                            <li class="active"><a href="index.html">Home</a></li>
                            <li><a href="gallery.html">Recipe gallery</a></li>
                            <li><a href="about.html">About</a></li>
                            <li><a href="contact.html">Contact</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="header-heading col-md-11 col-xs-12">
            <h2>Choose your favorite recipe and cooking with Audugesiri. Enjoy all the flavors with Adugesiri.</h2>
            </div>
        </div>
    </div>
    <!-- ===============================================
    END HEADER =================================== -->

    <!-- ===============================================
    START CONTENT =================================== -->
    <div class="content">
        <div class="container">
            <div class="page">
                <div class="filter-area">
					<div class="filter-box">
						<ul class="filter tesla_filters" data-tesla-plugin="filters">
							<?php
								include_once"scripts/connect.php";
								@mysql_connect("$db_host","$db_username","$db_pass") or die ("could not connect to mysql");
								@mysql_select_db("$db_name") or die ("no database");
								$categories = mysql_query("SELECT distinct(category) category from entries");
								while($row1 = mysql_fetch_array($categories)){
									$category = $row1["category"];
								?>
							<li><a data-category="<?php print $category;?>" 
									<?php if ($category == 'Curries') { ?> 
									class="active" 
									<?php } ?>
									href="#"><?php print $category;?></a></li>
							<?php } ?>                        
						</ul>
						<h1>Recipes</h1>
					</div>

					<div class="row" data-tesla-plugin="masonry">
					<?php
							include_once"scripts/connect.php";
							@mysql_connect("$db_host","$db_username","$db_pass") or die ("could not connect to mysql");
							@mysql_select_db("$db_name") or die ("no database");
							$entries = mysql_query("SELECT id, title, mainfile, category from entries");
							while($row1 = mysql_fetch_array($entries)){
								$category = $row1["category"];
								$mainfile = $row1["mainfile"];
								$id       = $row1["id"];
								$title    = $row1["title"];
							?>
						<div class="col-md-4 <?php print $category; ?> col-xs-6">
							<div class="recipe-item">
								<a href="images/cook/<?php print $mainfile;?>" class="swipebox"><span class="recipe-item-cover"><img src="images/cook/<?php print $mainfile;?>" alt="food image" /></span></a>
								<h1><a href="post.html?id=<?php print $id;?>"><?php print $title;?></a></h1>
							</div>
						</div>	
						<?php } ?>                    
					</div>
				</div>

            <ul class="page-numbers">
                <li><span class="page-numbers current">1</span> /</li>
                <li><a class="page-numbers" href="#">2</a> /</li>
                <li><a class="page-numbers" href="#">3</a> /</li>
                <li><a class="page-numbers" href="#">4</a> /</li>
                <li><a class="page-numbers" href="#">5</a> /</li>
                <li><a class="next page-numbers" href="#">→</a></li>
            </ul>

        </div>
    </div>
    <!-- ===============================================
    END CONTENT =================================== -->



    <!-- ===============================================
    START FOOTER =================================== -->
    <div class="container">
        <div class="footer">
            <p>Copyright © 2014 iCOOK Blog. All rights reserved. Vivamus gravida justo sed nisl viverra in venenatis lacus posuere.</p>
            <ul>
                <li><a href="about.html">About</a></li>
                <li><a href="recipe.html">Recipe Gallery</a></li>
                <li><a href="contact.html">Contact</a></li>
            </ul>
        </div>
    </div>
    <!-- ===============================================
    END FOOTER =================================== -->


        <!-- ======================================================================
                                        START SCRIPTS
        ======================================================================= -->
        <script src="js/modernizr.custom.63321.js" type="text/javascript"></script>
        <script src="js/jquery-1.10.0.min.js" type="text/javascript"></script>
        <script src="js/jquery-ui.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.js" type="text/javascript"></script>
        <script src="js/placeholder.js" type="text/javascript"></script>
        <script src="js/imagesloaded.pkgd.min.js" type="text/javascript"></script>
        <script src="js/masonry.pkgd.min.js" type="text/javascript"></script>
        <script src="js/jquery.swipebox.min.js" type="text/javascript"></script>
        <script src="js/options.js" type="text/javascript"></script>
        <script src="js/plugins.js" type="text/javascript"></script>
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <!-- ======================================================================
                                        END SCRIPTS
        ======================================================================= -->
    </body>
</html>