<?php
//This code was written by Freddie Sherratt
//and has been lisenced under the creative commons act.
//See http://creativecommons.org/licenses/by/2.0/uk/ for licsencing agreement.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

//The line of code below runs the connect script, this allows you to connect to your sql datebase throughout the rest of the code.
include_once"scripts/connect.php";
//Tells the computer only to run this code if the form has been submited

$parse_var = '';
$msg = '';


//The next 3 lines gather the information entered into form and turns them into php variables so they can be entered into the database
if (isset($_POST['parse_var'])){
	echo "entered first if";
  if ($_POST['parse_var'] == "new"){
	 	echo "entered second if";
	$name    = isset($_POST['name']) ? $_POST['name'] : '';
	$email   = isset($_POST['email']) ? $_POST['email'] : '';
	$website = isset($_POST['website']) ? $_POST['website'] : '';
	$comment = isset($_POST['comment']) ? $_POST['comment'] : '';	
	
	//$sqlcreate = mysql_query("INSERT INTO comments (name, email, website, comments)
	//VALUES('$name','$email','$website', '$comment')");
	
	$conn = new mysqli($db_host, $db_username, $db_pass, $db_name);
	$stmt = $conn->prepare("INSERT INTO comments (name, email, website, comments) VALUES(?,?,?,?)");
	$stmt->bind_param("ssss", $name, $email, $website, $comment);
	$stmt->execute();
	
	/*if ($sqlcreate){
		$msg = '<font color="#009900">A new comment has been posted.</font>';
	} else {
		$msg = '<font color="#FF0000">Problems connecting to server, please try again later.</font>';
	}*/
	
	echo "New records created successfully";

	$stmt->close();
	$conn->close();
  }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>iCOOK</title>
        <meta name="description" content="Here goes description" />
        <meta name="author" content="author name" />
        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
        <!-- ======================================================================
                                    Mobile Specific Meta
        ======================================================================= -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <!-- ======================================================================
                                    Style CSS + Google Fonts
        ======================================================================= -->
        <link href='http://fonts.googleapis.com/css?family=Oswald:400,700,300|Bitter:400,700,400italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/bootstrap.css" />    
        <link rel="stylesheet" href="css/style.css" />
    </head>
<body>
    <!-- ===============================================
    START HEADER =================================== -->
    <div class="header">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-xs-6">
                    <div class="logo">
                        <a href="index.html">Adugesiri</a>
                    </div>
                </div>
                <div class="col-md-8 col-xs-6">
                    <div class="menu">
                        <div class="responsive-menu">Menu</div>
                        <ul>
                            <li><a href="index.php">Home</a></li>
                            <li><a href="gallery.php">Recipe gallery</a></li>
                            <li><a href="about.php">About</a></li>
                            <li class="active"><a href="contact.php">Contact</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="header-heading">
            <h2>Your recipe is at a click away. Choose your favorite recipe and cooking with ADUGESIRI. Enjoy all the flavors with ADUGESIRI.</h2>
            </div>
        </div>
    </div>
    <!-- ===============================================
    END HEADER =================================== -->



    <!-- ===============================================
    START CONTENT =================================== -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="page">
                        <h3 class="site-title">I am easy to find</h3>
                        <div class="google-map">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d11910.7379386882!2d-93.59782812906528!3d41.72732676651727!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2s!4v1388999556984" width="100%" height="450" frameborder="0" style="border:0"></iframe>
                        </div>
                        <div class="contact-info">
                            <div class="row">
                                <div class="col-md-4">
                                    <ul>
                                        <li><span class="location">Guabbalala, Kanakapura Road</span></li>
                                    </ul>
                                </div>
                                <div class="col-md-4">
                                    <ul>
                                        <li><span class="mail"><a href="mailto:adugesiri@gmail.com">Adugesiri@gmail.com</a></span></li>
                                    </ul>
                                </div>
                                <div class="col-md-4">
                                    <ul>
                                        <li><span class="phone">(000) 355 588 456 789</span></li>
                                        <li><span class="phone">(777) 311 225 689 147</span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <h3 class="site-title">Write me a letter</h3>
                        <form id="contact_form" method="post" action="contact.php">
                            <div class="row">
                                <div class="col-md-4"><p>Name</p>
                                    <input type="text" name="contact-name" name="name" class="the-line">
                                </div>
                                <div class="col-md-4"><p>E-mail</p>
                                    <input type="text" name="contact-email" name="email" class="the-line">
                                </div>
                                <div class="col-md-4"><p>Website</p>
                                    <input type="text" name="contact-website" name="website" class="the-line">
                                </div>
                            </div>
                            <p>Message</p>
                            <textarea name="contact-message" class="the-area" name="comment"></textarea>
                            <p><input type="submit" id="form-send" value="Write the letter" class="contact-button"><input name="parse_var" type="hidden" value="new" /></p>
							<!--<p class="form-submit">
								<input name="submit" type="submit" id="submit" value="Write the letter" class="contact-button">
								<input type="hidden" name="comment_post_ID" value="" id="comment_post_ID">
								<input type="hidden" name="comment_parent" id="comment_parent" value="0">
								<input name="parse_var" type="hidden" value="new" />
                            </p>-->
                        </form>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <!-- ===============================================
    END CONTENT =================================== -->



    <!-- ===============================================
    START FOOTER =================================== -->
    <div class="container">
        <div class="footer">
            <p>Copyright © 2014 iCOOK Blog. All rights reserved. Vivamus gravida justo sed nisl viverra in venenatis lacus posuere.</p>
            <ul>
                <li><a href="about.html">About</a></li>
                <li><a href="recipe.html">Recipe Gallery</a></li>
                <li><a href="contact.html">Contact</a></li>
            </ul>
        </div>
    </div>
    <!-- ===============================================
    END FOOTER =================================== -->

        <!-- ======================================================================
                                        START SCRIPTS
        ======================================================================= -->
        <script src="js/modernizr.custom.63321.js" type="text/javascript"></script>
        <script src="js/jquery-1.10.0.min.js" type="text/javascript"></script>
        <script src="js/jquery-ui.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.js" type="text/javascript"></script>
        <script src="js/placeholder.js" type="text/javascript"></script>
        <script src="js/imagesloaded.pkgd.min.js" type="text/javascript"></script>
        <script src="js/masonry.pkgd.min.js" type="text/javascript"></script>
        <script src="js/jquery.swipebox.min.js" type="text/javascript"></script>
        <script src="js/options.js" type="text/javascript"></script>
        <script src="js/plugins.js" type="text/javascript"></script>
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <!-- ======================================================================
                                        END SCRIPTS
        ======================================================================= -->
    </body>
</html>