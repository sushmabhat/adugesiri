<!DOCTYPE HTML>
<html lang="en">
	<head>
	<meta charset="utf-8" />
	<title>Adugesiri</title>
	<meta name="description" content="Adugesiri contains loads of recipies across Karnataka and India" />
	<meta name="author" content="Reshma Bhat" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
	<!-- ======================================================================
								Mobile Specific Meta
	======================================================================= -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<!-- ======================================================================
								Style CSS + Google Fonts
	======================================================================= -->
	<link href='http://fonts.googleapis.com/css?family=Oswald:400,700,300|Bitter:400,700,400italic' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/bootstrap.css" />    
	<link rel="stylesheet" href="css/style.css" />
	<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {
		$("#results" ).load( "fetch_pages.php"); //load initial records
		
		//executes code below when user click on paginatn links
		$("#results").on( "click", ".paginatn a", function (e){
			e.preventDefault();
			$(".loading-div").show(); //show loading element
			var page = $(this).attr("data-page"); //get page number from link
			$("#results").load("fetch_pages.php",{"page":page}, function(){ //get content from PHP page
				$(".loading-div").hide(); //once done, hide loading element
			});
			
		});
	});
	</script>
	<style>

	.contents{
		margin: 20px;
		padding: 20px;
		list-style: none;
		background: #F9F9F9;
		border: 1px solid #ddd;
		border-radius: 5px;
	}
	.contents li{
		margin-bottom: 10px;
	}
	.loading-div{
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		background: rgba(0, 0, 0, 0.56);
		z-index: 999;
		display:none;
	}
	.loading-div img {
		margin-top: 20%;
		margin-left: 50%;
	}

	/* paginatn style */
	.paginatn{margin:0;padding:0;}
	.paginatn li{
		display: inline;
		padding: 6px 10px 6px 10px;
		border: 1px solid #ddd;
		margin-right: -1px;
		font: 15px/20px Arial, Helvetica, sans-serif;
		background: #FFFFFF;
		box-shadow: inset 1px 1px 5px #F4F4F4;
	}
	.paginatn li a{
		text-decoration:none;
		color: rgb(89, 141, 235);
	}
	.paginatn li.first {
		border-radius: 5px 0px 0px 5px;
	}
	.paginatn li.last {
		border-radius: 0px 5px 5px 0px;
	}
	.paginatn li:hover{
		background: #CFF;
	}
	.paginatn li.active{
		background: #F0F0F0;
		color: #333;
	}
	</style>
</head>
<body>
<!-- ===============================================
    START HEADER =================================== -->
    <div class="header">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-xs-6">
                    <div class="logo">
                        <a href="index.html">Adugesiri</a>
                    </div>
                </div>
                <div class="col-md-7 col-xs-6">
                    <div class="menu">
                        <div class="responsive-menu">Menu</div>
                        <ul>
                            <li class="active"><a href="index.html">Home</a></li>
                            <li><a href="gallery.html">Recipe gallery</a></li>
                            <li><a href="about.html">About</a></li>
                            <li><a href="contact.html">Contact</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="header-heading col-md-11 col-xs-12">
            <h2>Choose your favorite recipe and cooking with Audugesiri. Enjoy all the flavors with Adugesiri.</h2>
            </div>
        </div>
    </div>
    <!-- ===============================================
    END HEADER =================================== -->

<div class="content">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
					<div class="loading-div"><img src="ajax-loader.gif" ></div>
					<div id="results"><!-- content will be loaded here --></div>
				</div>	
				<div class="col-md-4">
                    <div class="sidebar">
                        <div class="widget">
                            <form class="search">
                                <input type="text" class="search-line" placeholder="What are you searching for?" name="serch">
                                <input type="submit" value="" class="search-button">
                            </form>
                        </div>	
						<div class="widget">
                            <h2 class="widget-title">Recent Recipies</h2>
                            <div class="popular-entrys">
								<?php
								
								include_once"scripts/connect.php";
								@mysql_connect("$db_host","$db_username","$db_pass") or die ("could not connect to mysql");
								@mysql_select_db("$db_name") or die ("no database");
								$populars   = mysql_query("select id, title, mainfile from entries");
								$i = 1;
								while(($row1 = mysql_fetch_array($populars)) && $i<=5) {
								?>
                                <div class="popular-entry">
                                    <a href="post.php?id=<?php $id = $row1["id"]; print "$id";?>"><img src="images/cook/<?php $mainfile = $row1["mainfile"]; print "$mainfile";?>" alt="blog image" /></a>
                                    <h3><a href="post.php?id=<?php echo $row1['id']; ?>"><?php $title = $row1["title"]; print "$title";?></a></h3>
                                </div>
								<?php									
								}
								?>                                
                            </div>
                        </div>

                        <div class="widget">
                            <h2 class="widget-title">Social</h2>
                            <ul class="socials">
                                <li><a href="#"><img src="images/mini_facebook.png" alt="facebook" /></a></li>
                                <li><a href="#"><img src="images/mini_twitter.png" alt="twitter" /></a></li>
                                <li><a href="#"><img src="images/mini_rss.png" alt="rss" /></a></li>
                                <li><a href="#"><img src="images/mini_google+.png" alt="google+" /></a></li>
                            </ul>
                        </div>

                        <div class="widget">
                            <h2 class="widget-title">Meta</h2>
                            <ul>
                                <li><a href="#">Log in</a></li>
                                <li><a href="#">Archive</a></li>
                            </ul>
                        </div>
				</div>
			</div>
		</div>
</div>
</body>
</html>
